***Programación***
5AVP
José Manuel Quintero Molina 

Practica #1 02/09/2022 - Practica de ejemplo
Commit: 1349d06e8bb523ef74a0aadc63d3b169ae91abd3
Archivo: https://gitlab.com/manuelqm06/desarrollo_aplicaciones_web/-/blob/main/parcial%201/practica_ejemplo.html

Practica #2 09/09/2022 - Práctica JavaScript 
Commit: 90115e3b210d75de891a7fb6c30ce32330872b4f
Archivo: https://gitlab.com/manuelqm06/desarrollo_aplicaciones_web/-/blob/main/parcial%201/Practica2-JavaScript.html

Practica #3 15/09/2022 - Práctica web con bases de datos -parte 1
Commit: 25b2cc79d5e777d8e424c7899caf88f983fd6a7d
Archivo:  https://gitlab.com/manuelqm06/desarrollo_aplicaciones_web/-/blob/main/parcial%201/bootstrap-4.6.2-dist__1_.zip

Practica #4  19/09/2022 - Práctica web con bases de datos - Vista de consulta de datos
Commit: 7b9cd1b44db49436e12e0f28e5b213d3017e1994

Practica #5 19/09/2022 - Práctica web con bases de datos - Vista de registro de datos
Commit: 2e6a9df1ae5f78db449062f30e6bfb14a1b9ad93
